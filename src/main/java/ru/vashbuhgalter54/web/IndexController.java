package ru.vashbuhgalter54.web;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.File;

@Controller
public class IndexController {

    @GetMapping({"/", "/index"})
    private String index() {
        return "index";
    }

    @GetMapping(value = "/sitemap.xml", produces = {"application/xml"})
    @ResponseBody
    public Resource getSitemap() {
        return new ClassPathResource("/static/sitemap.xml");
    }

}
