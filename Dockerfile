FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD /target/web-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Djava.net.preferIPv4Stack=true","-Djava.net.preferIPv4Addresses", "-jar","/app.jar"]
